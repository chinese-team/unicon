/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

 /**************************************************************************
      the GBK standard internal code is
          start at 33088  ===> 0x8140
          end   at 65279  ===> 0xfeff
                Arthur Ma   arthur.ma@turbolinux.com.cn
  */

#ifdef MODULE
#include <linux/module.h>
#include <linux/fb_doublebyte.h>
#else
#include <mytype.h>
#endif
#include <font_gbk16.h>
#define min1          0x81
#define max1          0xfe
#define min2          0x40
#define max2          0xff

static int index_gbk (int left, int right)
{
    int n;
    n = (left - 0x81) * 192;
    if (right <= 0xff && right >= 0x40)
        n += (right - 0x40);
    return n << 5;
}

static int is_hz_left(int c)
{
        return (c >= min1 && c <= max1);
}

static int is_hz_right(int c)
{
        return (c >= min2 && c <= max2); 
}

#ifdef MODULE
static struct double_byte db_gbk =
#else
struct double_byte db_gbk =
#endif
{
	0,
	"GBK",
        is_hz_left,
        is_hz_right,
	index_gbk,
	16,16,
	max_gbk16,
	font_gbk16
};

#ifdef MODULE
int init_module(void)
{
        if (UniconFontManager == NULL)
            return 1;
        if (UniconFontManager->registerfont (XL_DB_GBK, &db_gbk) == 0)
            return 1;
        return 0;
}

void cleanup_module(void)
{
        if (UniconFontManager == NULL)
            return;
        UniconFontManager->unregisterfont (XL_DB_GBK);
}	
#endif
