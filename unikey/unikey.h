/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#ifndef UNIDEV_H
#define UNIDEV_H

#include <linux/ioctl.h> 
// #include <video/fbcon.h> 

typedef struct __ChinesePut_T__
{
    int x, y;
    unsigned char c1, c2;
    long cl;
}
ChinesePut_T;

typedef struct __AsciiPut_T__
{
    int x, y;
    unsigned char ch;
    long cl;
}
AsciiPut_T;

typedef struct __VtInfo_T__
{
    int vt_has_resized;
    int width, height;
}
VtInfo_T;

typedef struct __VtFont_T__
{
    int tty;
    int font_type;
    int input_method_notify;
} VtFont_T;

#define DEVICE_FILE_NAME "/dev/unikey"
#define MAJOR_NUM 10
#define MINOR_NUM 202
#define UNIKEY_IOCTL 100
#define UNI_INPUT_GET_INFO    _IOR(UNIKEY_IOCTL, 0, char *)
#define UNI_INPUT_SET_INFO    _IOR(UNIKEY_IOCTL, 1, char *)
#define UNI_INPUT_REGISTER    _IOR(UNIKEY_IOCTL, 2, char *)
#define UNI_INPUT_UNREGISTER  _IOR(UNIKEY_IOCTL, 3, char *)

#define UNI_INPUT_PUT_ASCII   _IOR(UNIKEY_IOCTL, 4, AsciiPut_T *)
#define UNI_INPUT_PUT_CHINESE _IOR(UNIKEY_IOCTL, 5, ChinesePut_T *)
#define UNI_INPUT_CLS_BOTTOM  _IOR(UNIKEY_IOCTL, 6, char *)
#define UNI_INPUT_GET_VT_INFO _IOR(UNIKEY_IOCTL, 7, VtInfo_T *)

#define UNI_INPUT_SET_CUR_TTY _IOR(UNIKEY_IOCTL, 8, VtInfo_T *)
#define UNI_INPUT_SET_RESIZE_FLAG _IOR(UNIKEY_IOCTL, 9, char *)
#define UNI_INPUT_SET_UNRESIZE_FLAG _IOR(UNIKEY_IOCTL, 10, char *)
#define UNI_SET_CURRENT_FONT  _IOR(UNIKEY_IOCTL, 11, VtFont_T *)

#define MAXTTYS		6
#define SUCCESS         0
#define DEVICE_NAME     "unikey"
#define BUF_LEN         80
#define MAX_CHAR        8

/* op */
#define FLUSH_BOTTOM         1
#define FLUSH_INPUTMETHOD    2

typedef struct __TTY_KEY_T__
{
    u_char nTty;
    u_char op; //bFlushInput;
    u_char buf[MAX_CHAR];
    u_char nTotal;
}
TTY_KEY_T;
extern int nCurTty;
#endif

