/*
 * UNICON - The Console Chinese & I18N 
 * Copyright (c) 1999-2002 Arthur Ma <arthur.ma@turbolinux.com.cn>
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA, or contact Ralf S. Engelschall <rse@engelschall.com>.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 *
 * $Id$
 */

/* Imm Standard Interfaces */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <xl_hzinput.h>
#include <ImmModule.h>
#include <xl_pinyin.h>

extern int CCE_GetSelectDisplay (InputModule * p, char *buf);
extern int CCE_GetInputDisplay (InputModule * p, char *buf);
extern int Pinyin_ConfigureInputArea (InputModule * p, int SelectionLen0);
extern char *Pinyin_DoSelectItem (InputModule * pClient, int n, char *buf);
extern char *Pinyin_szGetSelItem (InputModule * pClient, int n, char *buf);
extern int Pinyin_KeyFilter (InputModule * pClient, u_char key,
			     char *buf, int *len);
extern int Pinyin_KeyPressed (InputModule * pClient, u_char key);
extern void CCE_ClosePinyin (InputModule * p);
extern InputModule *pCCE_OpenPinyin (char *szPath);
extern void Pinyin_SaveAllPyUsrPhrase ();

static void
SetPhraseBuffer (PhraseItem * p, char *buf, int buflen)
{
  char *p1 = buf;

  p->szKeys = p1;
  p1 += 32;

  p->KeyLen = (u_char *) p1;
  p1 += sizeof (*p->KeyLen);

  p->frequency = (freq_t *) p1;
  p1 += sizeof (freq_t);

  p->szPhrase = p1;
}

static IMM_CLIENT *
IMM_open (char *szFileName, long type)
{
  IMM_CLIENT *q;
  InputModule *a;

  a = (InputModule *) pCCE_OpenPinyin (szFileName);
  if (a == NULL)
    return NULL;

  q = (IMM_CLIENT *) malloc (sizeof (IMM_CLIENT));
  if (q == NULL)
    return NULL;
  q->pImmClientData = (void *) a;
  SetPhraseBuffer (&q->m, q->buf, sizeof (q->buf));
  return q;
}

static int
IMM_save (IMM_CLIENT * p, char *szFileName)
{
  return 1;
}

static int
IMM_close (IMM_CLIENT * p)
{
  InputModule *pClient = (InputModule *) p->pImmClientData;
  CCE_ClosePinyin (pClient);
  free (p);
  return 1;
}

/* Indepent Modules support */
int
IMM_KeyFilter (IMM_CLIENT * p, u_char key, char *buf, int *len)
{
  return Pinyin_KeyFilter ((InputModule *) p->pImmClientData, key, buf, len);
}

/* Input Area Configuration & Operation */
static int
IMM_ConfigInputArea (IMM_CLIENT * p, int SelectionLen)
{
  return Pinyin_ConfigureInputArea (
				    (InputModule *) p->pImmClientData,
				    SelectionLen);
}

static int
IMM_GetInputDisplay (IMM_CLIENT * p, char *buf, long buflen)
{
  return CCE_GetInputDisplay ((InputModule *) p->pImmClientData, buf);
}

static int
IMM_GetSelectDisplay (IMM_CLIENT * p, char *buf, long buflen)
{
  return CCE_GetSelectDisplay ((InputModule *) p->pImmClientData, buf);
}

int
IMM_ResetInput (IMM_CLIENT * p)
{
  /* Send Esc */
  return Pinyin_KeyPressed ((InputModule *) p->pImmClientData, '\033');
}

PhraseItem *
IMM_pGetItem (IMM_CLIENT * p, u_long n)
{
  InputModule *pClient = (InputModule *) p->pImmClientData;
  char *s = Pinyin_szGetSelItem (pClient, (int) n, p->m.szPhrase);
  if (s == NULL)
    return NULL;
  return &p->m;
}

/* Phrase Operation */
static int
IMM_AddPhrase (IMM_CLIENT * pClient, PhraseItem * p)
{
  return 1;
}

static int
IMM_ModifyPhraseItem (IMM_CLIENT * p, long n, PhraseItem * pItem)
{
  return 1;
}

static int
IMM_Flush ()
{
  return 1;
}

#ifdef  __cplusplus
extern "C"
#endif
struct ImmOperation ImmOp_Ptr = {
  "ƴ��",
  "Free Pinyin Version 1.0",
  "Author: ZhenChun Li",
  IMM_CCE | IMM_LC_GB2312 << 24,
  IMM_open,
  IMM_save,
  IMM_close,
  /* Indepent Modules support */
  IMM_KeyFilter,
  IMM_ResetInput,

  /* Input Area Configuration & Operation */
  IMM_ConfigInputArea,
  IMM_GetInputDisplay,
  IMM_GetSelectDisplay,

  IMM_pGetItem,
  IMM_AddPhrase,
  IMM_ModifyPhraseItem,
  IMM_Flush,
};
