
/* 	
			UNICON PROJECT

   		    Copyright (C) 1999 TLDN
	
   Authors:	Li Qi Chen <chrisl@turbolinux.com.cn>
		Arthur Ma  <arthur.ma@turbolinux.com.cn>
		Justin Yu  <justiny@turbolinux.com.cn>

   Homepage:    http://turbolinux.com.cn/TLDN/chinese/project/unicon/index.html
   Mailinglist: unicon@turbolinux.com.cn 	
   Download:    http://turbolinux.com.cn/TLDN/chinese/project/unicon/download.html

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

/* Imm Standard Interfaces */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Phrase.h>
#include <ImmModule.h>
#include <hzinput.h>

void SetPhraseBuffer (PhraseItem *p, char *buf, int buflen)
{
    char *p1 = buf;

    p->szKeys = p1;
    p1 += 32;

    p->KeyLen = (u_char *) p1;
    p1 += sizeof (*p->KeyLen);

    p->frequency = (u_short *) p1;
    p1 += sizeof (*p->frequency);

    p->szPhrase = p1;
}

static IMM_CLIENT *IMM_open (char *szFileName)
{
    IMM_CLIENT *q;
    HZIPT *a;

    a = Cxterm_Open (szFileName);
    if (a == NULL)
        return NULL;
    
    q = (IMM_CLIENT *) malloc (sizeof (IMM_CLIENT));
    if (q == NULL)
    {
        free (a);
        return NULL;
    };
    SetPhraseBuffer (&q->m, q->buf, sizeof (q->buf));
    q->pImmClientData = (void *) a;
    return q;
}

static int IMM_save (IMM_CLIENT *p, char *szFileName)
{
    return 1;
}

static int IMM_close (IMM_CLIENT *p)
{
    HZIPT *pClient = (HZIPT *) p->pImmClientData; 
    Cxterm_Close (pClient);
    free (p);
    return 1;
}

/* Indepent Modules support */
static int IMM_KeyFilter (IMM_CLIENT *p, u_char key, char *buf, int *len)
{
    return Cxterm_HzFilter ((HZIPT *) p->pImmClientData, key, buf, len);
}

/* Input Area Configuration & Operation */
static int IMM_ConfigInputArea (IMM_CLIENT *p, int SelectionLen)
{
    return 1;
}

static int IMM_GetInputDisplay (IMM_CLIENT *p, char *buf, long buflen)
{
    return Cxterm_GetInputDisplay ((HZIPT *) p->pImmClientData, buf);
}

static int IMM_GetSelectDisplay (IMM_CLIENT *p, char *buf, long buflen)
{
    return Cxterm_GetSelectionDisplay ((HZIPT *) p->pImmClientData, buf);
}

int IMM_ResetInput (IMM_CLIENT *p)
{
    char buf[32];
    int len;
    /* Send Esc */
    return (long) Cxterm_HzFilter 
           ((HZIPT *) p->pImmClientData, '\n', buf, &len);
    return 1;
}

PhraseItem * IMM_pGetItem (IMM_CLIENT *p, u_long n)
{
    HZIPT *pClient = (HZIPT *) p->pImmClientData; 
    char *s = szGetSelItem (pClient, (int) n, p->m.szPhrase);
    if (s == NULL)
         return NULL;
    return &p->m;
}

/* Phrase Operation */
static int IMM_AddPhrase (IMM_CLIENT *pClient, PhraseItem *p)
{
    return 1;
}

static int IMM_ModifyPhraseItem (IMM_CLIENT *p, long n, PhraseItem *pItem)
{
    return 1;
}

static int IMM_Flush ()
{
    return 1;
}

#ifdef  __cplusplus
extern "C" 
#endif
struct ImmOperation ImmOp_Ptr = {
    "ƴ��", 
    "Cxterm Driver V1.0",
    "Author: TLC Group",
    IMM_CCE,
    IMM_open,
    IMM_save,
    IMM_close,

    /* Indepent Modules support */
    IMM_KeyFilter,
    IMM_ResetInput,

    /* Input Area Configuration & Operation */
    IMM_ConfigInputArea,
    IMM_GetInputDisplay,
    IMM_GetSelectDisplay,
    IMM_pGetItem,
    IMM_AddPhrase,
    IMM_ModifyPhraseItem,
    IMM_Flush,
};

#ifdef __CCE_HZINPUT_DEBUG__
int test (long a, PhraseItem *p)
{
    printf ("%s\n", p->szPhrase);
    return 1;
}

/*
    gcc -g -I../include -I. -D__CCE_HZINPUT_DEBUG__ xl_hzinput.c CCE_hzinput.c
*/

int main ()
{
    IMM_CLIENT *pImm;
    char *szTest = "wa"; //ng";
    int i, j;
    long n;
    PhraseItem *p; 
    PhraseItem a; 

    pImm = ImmOp_Ptr.open ("../../datas"); ///pinyin.tab");
                           //"../../datas/ziranma.tab");
    pImm->m.szPhrase = pImm->buf;

    printf ("\n");
    for (i = 0; i < strlen (szTest); i++)
    {
        char buf[256];
        n = ImmOp_Ptr.KeyFilter (pImm, szTest[i]);
        ImmOp_Ptr.GetInputDisplay (pImm, buf, sizeof (buf)),
        printf ("Input::%s\n", buf);
        ImmOp_Ptr.GetSelectDisplay (pImm, buf, sizeof (buf));
        printf ("Selection::%s\n", buf);

        printf ("each select test...\n");
        for (j = 0; j < n; j++)
        {
             p = IMM_pGetItem (pImm, j);
             if (p != NULL)
                 printf ("%s,", p->szPhrase);
        }
        printf ("\n");
    }

    p = ImmOp_Ptr.DoSelectItem (pImm, 0);
    if (p != NULL)
        printf ("you select : 0 ==> %s\n", p->szPhrase);
    printf ("resuming....\n");
    n = ImmOp_Ptr.RestoreToLastStep (pImm);
    for (j = 0; j < n; j++)
    {
        p = IMM_pGetItem (pImm, j);
        if (p != NULL)
            printf ("%s,", p->szPhrase);
    }
    printf ("\n");

    ImmOp_Ptr.PageUp (pImm);
    ImmOp_Ptr.PageDown (pImm);

    ImmOp_Ptr.close (pImm);
    return 1;
}
#endif

