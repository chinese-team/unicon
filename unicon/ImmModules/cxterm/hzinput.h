/* hzinput.h */

/***********************************************************************
* Copyright 1994 by Yongguang Zhang.

Permission to use, copy, modify, and distribute this software and
its documentation for any purpose and without fee is hereby granted, 
provided that the above copyright notice appear in all copies and
that both that copyright notice and this permission notice appear
in supporting documentation, and that the names of the authors not
used in advertising or publicity pertaining to distribution of the
software without specific, written prior permission.

THE AUTHOR(S) DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN
NO EVENT SHALL THE AUTHOR(S) BE LIABLE FOR ANY SPECIAL, INDIRECT OR
CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE
USE OR PERFORMANCE OF THIS SOFTWARE.

******************************************************************/

#ifndef __HZINPUT_H__
#define __HZINPUT_H__

#define NR_ENCODING	2

#define GB_ENCODING	0
#define BIG5_ENCODING	1

/* key of toggle input method */

#define NR_INPUTMETHOD	10

#define MAX_INPUT_BUF	32
#define MAX_PROMPT	24
#define MAX_KEYPROMPT	4
#define MAX_CHOICE	16

#define CIT_VERSION	2

/* key type */
#define HZKEY_INVALID		0
#define HZKEY_SELECTION_MASK	0x10
#define HZKEY_SELECTION_NUM	0x0f
#define HZKEY_INPUT_MASK	0x20
#define HZKEY_BACKSPACE		0x40
#define HZKEY_DELETEALL		0x41
#define HZKEY_RIGHT		0x80
#define HZKEY_LEFT		0x81
#define HZKEY_REEPEAT		0xc0

#define HZKEY_FORCE_SELECT	'\033'
typedef struct {		/* normal 16 bit characters are two bytes */
    unsigned char byte1;
    unsigned char byte2;
} XChar2b;

struct key_prompt
{
  unsigned char prompt[MAX_KEYPROMPT];
  unsigned short prompt_len;
};

struct trie_node
{
  unsigned char key;
  unsigned char num_next_keys;
  unsigned short int num_choice;
  unsigned int next_keys;
  unsigned int hz_index;
};

struct hz_input_table
{
    unsigned char	version;	/* version num of this .cit format */
    char		encode;		/* Chinese encoding: GB or BIG5 */
    unsigned char	builtin;	/* this table is builtin? */

    unsigned int	trie_list_size;	/* size of the whole TRIE list */
    unsigned int	hz_list_size;	/* size of the whole HZ list */
    struct trie_node	*trie_list;	/* start address of the Trie list */
    XChar2b		*hz_list;	/* start address of the HZ list */

    unsigned char	auto_select;	/* auto selection if single choice? */
    unsigned short	keytype[128];	/* valid type of the key */
    struct key_prompt	keyprompt[128];	/* display this for the key */

    char		choicelb[MAX_CHOICE];	/* HZ choice label */
    int			maxchoice;	/* maximum number of HZ choice */
    unsigned char	prompt[MAX_PROMPT];
    int			prompt_len;		/* len of the Prompt */
};

/* Cxterm HzInput Client Data */
typedef struct __Cxterm_Buf__
{
    int trans;           /* 0 for ascii, 1 for hanzi input */
    struct hz_input_table *input_table;
    int num_inputkey,
        current_pos,
        choice_start;
    unsigned char inputkey_buf[MAX_INPUT_BUF];
    struct trie_node *trie_node_buf[MAX_INPUT_BUF],
                     *current_trie_node;
    int force_select;
    char OutputBuf_input[64];
    char OutputBuf_selection[256];
    char OutputBuf_write[64];
    int nTotalCurSel;
} HZIPT;

extern HZIPT *Cxterm_Open (unsigned char * filename);
extern void Cxterm_Close (HZIPT *pClient);
extern int Cxterm_HzFilter (HZIPT *pClient, 
                 unsigned char c, char *buf, int *len);
extern int Cxterm_GetInputDisplay (HZIPT *pClient, char *buf);
extern int Cxterm_GetSelectionDisplay (HZIPT *pClient, char *buf);
extern char *szGetSelItem (HZIPT *pClient, int n, char *buf);

#endif
