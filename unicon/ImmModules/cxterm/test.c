#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <hzinput.h>
#include <ImmModule.h>

extern struct ImmOperation ImmOp_Ptr;
int test (long a, PhraseItem *p)
{
    printf ("%s\n", p->szPhrase);
    return 1;
}

/*
    gcc -g -I../include -I. -D__CCE_HZINPUT_DEBUG__ xl_hzinput.c CCE_hzinput.c
*/

int main ()
{
    IMM_CLIENT *pImm;
    char *szTest = "wa"; //ng";
    int i, j;
    long n;
    PhraseItem *p; 
    PhraseItem a; 

    pImm = ImmOp_Ptr.open ("./dict/CCDOSPY.cit"); ///pinyin.tab");
                           //"../../datas/ziranma.tab");
    pImm->m.szPhrase = pImm->buf;

    printf ("\n");
    for (i = 0; i < strlen (szTest); i++)
    {
        char buf[256];
        n = ImmOp_Ptr.KeyFilter (pImm, szTest[i]);
        ImmOp_Ptr.GetInputDisplay (pImm, buf, sizeof (buf)),
        printf ("Input::%s\n", buf);
        ImmOp_Ptr.GetSelectDisplay (pImm, buf, sizeof (buf));
        printf ("Selection::%s\n", buf);

        printf ("each select test...\n");
        for (j = 0; j < n; j++)
        {
             p = ImmOp_Ptr.pGetItem (pImm, j);
             if (p != NULL)
                 printf ("%s,", p->szPhrase);
        }
        printf ("\n");
    }

    p = ImmOp_Ptr.DoSelectItem (pImm, 0);
    if (p != NULL)
        printf ("you select : 0 ==> %s\n", p->szPhrase);
    printf ("resuming....\n");
    n = ImmOp_Ptr.RestoreToLastStep (pImm);
    for (j = 0; j < n; j++)
    {
        p = ImmOp_Ptr.pGetItem (pImm, j);
        if (p != NULL)
            printf ("%s,", p->szPhrase);
    }
    printf ("\n");

    ImmOp_Ptr.PageUp (pImm);
    ImmOp_Ptr.PageDown (pImm);

    ImmOp_Ptr.close (pImm);
    return 1;
}

