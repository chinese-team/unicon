/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2002
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#include <stdio.h>
#include <stdarg.h> 
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <xl_hzinput.h>
#include <xl_sysphrase.h>
#include <xl_phrase.h>
#include <test.h>

FILE *fr, *fw;
int lineno;
TL_SysPhrase_T *pDefaultSysphrase = NULL;

void print_error(char *fmt,...)
{
    va_list args;

    va_start(args, fmt);
    vfprintf(stderr, fmt, args);
    va_end(args);
    fprintf(stderr,"\n");
    exit(-1);
}

char *skip_space(char *s)
{
    while ((*s==' ' || *s=='\t') && *s) s++;
    return s;
}

char *to_space(char *s)
{
    while (*s!=' ' && *s!='\t' && *s) s++;
    return s;
}

void del_nl_space(char *s)
{
    char *t;

    int len = strlen (s);
    if (!*s) 
        return;
    t = s + len - 1;
    while ((*t=='\n' || *t==' ' || *t=='\t') && s<t) 
        t--;
    *(t+1)=0;
}


void get_line(u_char *tt)
{
    while (! feof(fr)) 
    {
        fgets (tt, 128, fr);
    lineno++;
        if (tt[0] == '#') 
            continue;
        else  
            break;
    }
}

void cmd_arg(u_char *s, u_char **cmd, u_char **arg)
{
    char *t;

    get_line(s);
    if (!*s) { *cmd=*arg=s; return; }

    s=skip_space(s);
    t=to_space(s);
    *cmd=s;
    if (!(*t)) {
    	*arg=t;
    	return;
    }
    *t=0;
    t++;
    t=skip_space(t);
    del_nl_space(t);
    *arg=t;
}

#define MAXSIZE            65535
/* maximum char/phrase can be defined */

ITEM            ItemOut[MAXSIZE];
// AssociatePhrase PhraseItem[MAXSIZE];
static char *TL_SYSPHRASE_FILE = "tl_sysphrase.bin";

count_t  GetPhraseLevel (long count)
{
    int i;
    for (i = 1; i < 256; i++)
    {
        if (count >= PhraseLevel[i-1] && count < PhraseLevel[i])
            return i - 1;
    }
    return 255; 
}

int  ProcessingSystemPhrase (char *szFileName)
{
    FILE *fp;
    char buf[256];
    u_char *cmd, *arg, *s;
    u_long count;
    count_t t;
    if ((fp = fopen (szFileName, "rt")) == NULL)
    {
        perror (szFileName);
        exit (0);
    }
    fr = fp;
    while (! feof (fp))
    {
        cmd_arg (buf, &cmd, &arg);
        if (!cmd[0] || !arg[0])
            break;
        if (isdigit (cmd[0]))
        {
            count = atol (cmd);
            s = arg;
        }
        else
        {
            count = atol (arg);
            s = cmd;
        }  
        t = GetPhraseLevel (count);
        TL_AddPhrase (pDefaultSysphrase, s, t);
    }
    fclose (fp); 
}

int main (int argc, char **argv)
{
    char *szWordPhraseFileName = NULL;
    char *szCharPhraseFileName = NULL;
    char *sysdumpfilename = NULL;
    int opt;
    extern char *optarg;
    extern int optind;

    if (argc<=1) 
    {
        printf ("%s [-o <SysPhrase File>] \n \
                    \t -c <char filename> \n \
                    \t -w <word phrase filename> \n \
                    \t -d <dump SysPhrase File> \n", 
                    argv[0]);
        exit (0);
    }

#define ACCEPTOPTS      "o:c:d:"
    while ((opt = getopt(argc, argv, ACCEPTOPTS)) != -1) 
    {
#undef ACCEPTOPTS
        switch (opt) 
        {
            case 'o':
                TL_SYSPHRASE_FILE = optarg;
                break;
            case 'c':
                szCharPhraseFileName = optarg;
                break; 
            case 'w':
                szWordPhraseFileName = optarg;
                break;
            case 'd':
                sysdumpfilename = optarg;
                break;
            default:
                printf ("Error in Parameter \n");
                exit (0);
        } 

    }
    pDefaultSysphrase = LoadSystemPhrase (TL_SYSPHRASE_FILE, 0);

    if (szWordPhraseFileName != NULL)
        ProcessingSystemPhrase (szWordPhraseFileName);
    if (szCharPhraseFileName != NULL)
        ProcessingSystemPhrase (szCharPhraseFileName);

    if (sysdumpfilename != NULL)
        TL_DumpAllPhrase (pDefaultSysphrase, sysdumpfilename);
    TL_SaveAllPhrase (pDefaultSysphrase, TL_SYSPHRASE_FILE);
    return 0;
}

