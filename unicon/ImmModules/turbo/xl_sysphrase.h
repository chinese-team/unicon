/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2002
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#ifndef __XL_SYSPHRASE_H__
#define __XL_SYSPHRASE_H__

#define USER_PHRASE_DEFAULT_COUNT                0
#define DEFAULT_FREQ                             0
#define USER_PHRASE_FREQUENCY_MAX                254
#define USER_SELECT_COUNT_MAX                    254

#include <Phrase.h>

typedef struct __PureSystemPhrase__
{
    freq_t   frequency;
    count_t  user_select_count;
    char *szPhrase;
	int next;
}
PureSystemPhrase;

typedef struct __SystemPhraseIndex__
{
    long offset;
    u_char len;
}
SystemPhraseIndex;

typedef struct __SysPhrase_Handle__
{
    PureSystemPhrase *pPhrase;
    long TotalPhrase;
} TL_SysPhrase_T;

TL_SysPhrase_T *LoadSystemPhrase (char *szFileName, int type);
int  UnloadSystePhrase (TL_SysPhrase_T *p);
int  TL_MatchPhrase (TL_SysPhrase_T *p, char *szPhraseg, 
                     u_long *freq, long *nPhrase);
int  TL_GetPhraseFreq (TL_SysPhrase_T *p, long nPhrase, u_long *freq);
int  TL_GetUserSelectPhraseCount (TL_SysPhrase_T *p, 
                                  long nPhrase, u_long *count);
int  TL_AdjustPhraseOrder (TL_SysPhrase_T *p, long nPhrase);
int  TL_GetPhrase (TL_SysPhrase_T *p, u_long nPhrase, char *buf);
long TL_AppendPhrase (TL_SysPhrase_T *p, char *buf);
long TL_AddPhrase (TL_SysPhrase_T *p, char *buf, u_long freq);
int  TL_DeletePhrase (TL_SysPhrase_T *p, char *buf);
int  TL_SaveAllPhrase (TL_SysPhrase_T *p, char *szFileName);
int  TL_DumpAllPhrase (TL_SysPhrase_T *p, char *szFileName);

#endif

