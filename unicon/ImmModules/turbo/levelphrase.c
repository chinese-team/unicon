/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2002
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#include <stdio.h>
#include <stdarg.h> 
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include "xl_hzinput.h"

FILE *fr, *fw;
int lineno;

void print_error(char *fmt,...)
{
    va_list args;

    va_start(args, fmt);
    vfprintf(stderr, fmt, args);
    va_end(args);
    fprintf(stderr,"\n");
    exit(-1);
}

char *skip_space(char *s)
{
    while ((*s==' ' || *s=='\t') && *s) s++;
    return s;
}

char *to_space(char *s)
{
    while (*s!=' ' && *s!='\t' && *s) s++;
    return s;
}

void del_nl_space(char *s)
{
    char *t;

    int len = strlen (s);
    if (!*s) 
        return;
    t = s + len - 1;
    while ((*t=='\n' || *t==' ' || *t=='\t') && s<t) 
        t--;
    *(t+1)=0;
}


void get_line(u_char *tt)
{
    while (! feof(fr)) 
    {
        fgets (tt, 128, fr);
    lineno++;
        if (tt[0] == '#') 
            continue;
        else  
            break;
    }
}

void cmd_arg(u_char *s, u_char **cmd, u_char **arg)
{
    char *t;

    get_line(s);
    if (!*s) { *cmd=*arg=s; return; }

    s=skip_space(s);
    t=to_space(s);
    *cmd=s;
    if (!(*t)) {
    	*arg=t;
    	return;
    }
    *t=0;
    t++;
    t=skip_space(t);
    del_nl_space(t);
    *arg=t;
}

#define MAXSIZE            1000000ul
/* maximum char/phrase can be defined */
u_char  PhraseMark[MAXSIZE];
u_long  Result[256];
int  ProcessingSystemPhrase (char *szFileName)
{
    FILE *fp;
    char buf[256];
    u_char *cmd, *arg, *s;
    u_long count;

    if ((fp = fopen (szFileName, "rt")) == NULL)
    {
        perror (szFileName);
        exit (0);
    }
    fr = fp;
    while (! feof (fp))
    {
        cmd_arg (buf, &cmd, &arg);
        if (!cmd[0] || !arg[0])
            break;
        if (isdigit (cmd[0]))
        {
            count = atol (cmd);
            s = arg;
        }
        else
        {
            count = atol (arg);
            s = cmd;
        }  
        if (count < MAXSIZE)
            PhraseMark[count] ++;
    }
    fclose (fp); 
}

void ProcessResult ()
{
    long n = 0, m, i, t;
    int nstep = 0;

    memset (Result, 0, sizeof (Result));
    for (i = 0; i < MAXSIZE; i++)
        if (PhraseMark[i] != 0)
            n++;
    printf ("Total = %d\n", n);
    nstep = n / 256;
    for (t = 0, m = 0, i = 0; i < MAXSIZE; i++)
    {
        if (PhraseMark[i] != 0)
        {
            if (m % nstep == 0)
                 Result[t++] = i;
            m++;
        }
    } 
}

void WriteAllResult (char *szFileName)
{
    FILE *fp;
    int i;
    fp = fopen (szFileName, "wt");
    fprintf (fp, "PhraseLevel[256] = { \n    ");
    for (i = 0; i  < 256; i++)
    {
        if (i % 16 == 0)
            fprintf (fp, "\n    ");
        fprintf (fp, "%ld,", Result[i]);
    }
    fprintf (fp, "};");
    fclose (fp);
}

int main (int argc, char **argv)
{
    int i;
    memset (PhraseMark, 0, MAXSIZE);
    for (i = 1; i < argc; i++)
        ProcessingSystemPhrase (argv[i]);
    ProcessResult ();
    WriteAllResult ("test.h");
    return 0;
}

