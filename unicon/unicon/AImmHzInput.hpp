/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#ifndef __AHZINPUT_HPP__
#define __AHZINPUT_HPP__
#include <MyInputArea.hpp>
#include <ImmClient.h>
#include <Phrase.h>
#include <MyConfig.hpp>
#include <UniKey.hpp>
#include <ConfigManager.hpp>

#define  MAX_SELECT_ITEM   10

class CAImmHzInput 
{
/*
private:
    char TmpBuf[8192 * 2];
    PhraseItem TmpPhraseItem[MAX_SELECT_ITEM];
*/
protected:
    /* Key Hooker */
    CUniKey *pCMyKey; 
    int nTty;
    int nTotalSelection;

    /* Input Area Operator */
    CMyInputArea  *pCMyInputArea;

    /* Configuration */
    CConfigManager *pMyConfig; 

    /* Server Operating Handler */
    ImmServer_T ImmServer;
    IMM *pImm;

    /* Input State Indicators */
    char bShowFirstStartMsg,
         IsHelpMenu,
         IsSysMenu,
         IsFullChar,
         IsHanziInput,
         IsFullComma;
    char IsHanziInputBackup,
         IsFullCharBackup,
         IsFullCommaInChinese,
         IsFullCommaInEnglish;

    /* Help Items Indicator */
    int  ShowTipItem;

    /* Make Phrase state Indicator */
    int IsMakingPhrase;
    char szUserPhraseCode[32];
    char szUserPhrase[256];
    char szOldMethod[256];
private:
    int  FillMatchesToShowBuffer ();
    void RefreshPhraseShowTable ();
    bool UpAddPhraseToShow (PhraseItem *p); 
    bool DownAddPhraseToShow (PhraseItem *p); 
protected:
    void ResetCoding (int coding, int Tty);
    int  OpenInputMethod (char *szImmModule, char *szImmTable, 
                         u_long type, char *szMethod);
    int  CloseInputMethod ();
    void ResetInput ();
    int  RefreshInputMode ();
public:
    CAImmHzInput (int nTty, ImmServer_T ImmServer, 
                  CConfigManager *pMyConfig,
                  CUniKey *pCMyKey0);
    virtual ~CAImmHzInput ();

    /* Key Filter control */
    int KeyFilter (u_char key, char *buf, int *len);

    /* Display Control */
    void RefreshInputArea();

    /* Make Phrase Support */
    int  MakingUserPhrase (char *szCode, char *szPhrase);
    void GetInputDisplay (char *szCode, int len);
    int  WriteUserDefinedPhrase (char *szPhrase, int len);
    void DoFlush ();
};

#endif

