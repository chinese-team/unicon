/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#include <iostream.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ImmDefs.h>
#include <MyConfig.hpp>

CMyConfig::CMyConfig (char *szConfigFileName0, char *szLanguageName0)
{
    TotalMethod = 0;
    szFullChar = NULL;
    szSingleChar = NULL;
    szSingleSymbol = NULL;
    szFullSymbol = NULL;
    szVersionInfo = NULL;
    szLanguageCode = NULL;
    szFirstMessage = NULL;
    szFullSymbolLeftMark = NULL;
    szFullSymbolRightMark = NULL;
    szSingleSymbolLeftMark = NULL;
    szSingleSymbolRightMark = NULL;

    /* Help defination */
    pszHelp = (char **) NULL;
    TotalHelp = 0;

    /* SysMenu Support */
    szSysMenu = NULL;
    memset (aInputMethod, 0, sizeof (aInputMethod));
    szConfigFileName = strdup (szConfigFileName0);
    szLanguageName = strdup (szLanguageName0); 
    LoadLanguageCode ();
}

CMyConfig::~CMyConfig ()
{
    int i;
    if (TotalMethod != 0)
    { 
        for (i = 0; i < TotalMethod; i++)
        {
            FreeString (aInputMethod[i].szImmModule);
            FreeString (aInputMethod[i].szImmTable);
            FreeString (aInputMethod[i].szMethodName);
        }
        TotalMethod = 0;
    }
    FreeString (szFullSymbolLeftMark);
    FreeString (szFullSymbolRightMark);
    FreeString (szSingleSymbolLeftMark);
    FreeString (szSingleSymbolRightMark);
    FreeString (szConfigFileName);
    FreeString (szLanguageCode);
    /* IMM User Interface  defination */
    FreeString (szFullChar);
    FreeString (szSingleChar);
    FreeString (szSingleSymbol);
    FreeString (szFullSymbol);
    FreeString (szVersionInfo);
    FreeString (szLanguageCode);
    FreeString (szFirstMessage);

    /* Help defination */
    if (pszHelp != NULL)
    {
        for (i = 0; i < TotalHelp; i++)
             free (pszHelp[i]);
        free (pszHelp);
        pszHelp = NULL;
        TotalHelp = 0;
    }
    /* SysMenu Support */
    FreeString (szSysMenu);
}

void CMyConfig::FreeString (char *p)
{
    if (p != NULL)
        free (p);
}

char *CMyConfig::szLoadSysConfigString (CSysConfig *pConfig,
                            char *p, char *szCapName, char *szDefault)
{
     FreeString (p);
     p = pConfig->szReadCapItem (szLanguageName, szCapName);
     if (p == NULL)
         p = szDefault;
     //cout << "Cap:" << szCapName << ":" << p << ":" << szDefault << "\n";
     if (p != NULL)
         return strdup (p);
     return NULL;
}

long CMyConfig::GetLanguageCode (char *szLanguageCode)
{
    if (strcasecmp (szLanguageCode, "gb") == 0)
        return IMM_LC_GB2312;
    else if (strcasecmp (szLanguageCode, "big5") == 0)
        return IMM_LC_BIG5;
    else if (strcasecmp (szLanguageCode, "jis") == 0)
        return IMM_LC_JISX0208;
    else if (strcasecmp (szLanguageCode, "kscm") == 0)
        return IMM_LC_KSC5601;
    else if (strcasecmp (szLanguageCode, "gbk") == 0)
        return IMM_LC_GBK;
    else if (strcasecmp (szLanguageCode, "gb18030") == 0)
        return IMM_LC_GB18030;
    else
    {
        printf ("Wrong Language Code = %s\n", szLanguageCode);
        exit (0);
    }
    return 0;
}

bool CMyConfig::LoadLanguageCode ()
{
    CSysConfig MyConfig (szConfigFileName);
    char *p;
    long LanguageCode;

    szFullSymbolLeftMark = szLoadSysConfigString 
                        (&MyConfig, szFullChar, "FullSymbolLeftMark", "[");
    szFullSymbolRightMark = szLoadSysConfigString
                        (&MyConfig, szFullChar, "FullSymbolRightMark", "]");
    szSingleSymbolLeftMark = szLoadSysConfigString
                        (&MyConfig, szFullChar, "SingleSymbolLeftMark", "[");
    szSingleSymbolRightMark = szLoadSysConfigString
                        (&MyConfig, szFullChar, "SingleSymbolRightMark", "]");

    szFullChar = szLoadSysConfigString 
                        (&MyConfig, szFullChar, "FullChar", "ȫ��");
    szSingleChar = szLoadSysConfigString (&MyConfig, szSingleChar, 
                                          "SingleChar", "Ӣ��");
    szSingleSymbol = szLoadSysConfigString (&MyConfig, szSingleSymbol, 
                                          "SingleSymbol", "Ӣ��");
    szFullSymbol = szLoadSysConfigString (&MyConfig, szFullSymbol, 
                                          "FullSymbol", "Ӣ��");
    szVersionInfo = szLoadSysConfigString (&MyConfig, szVersionInfo, 
                                          "VersionInfo", "Unicon 3.0.4");
    szLanguageCode = szLoadSysConfigString (&MyConfig, szLanguageCode, 
                                           "LanguageCode", "GB");
    szFirstMessage = szLoadSysConfigString (&MyConfig, szFirstMessage, 
                                           "FirstMessage", "");

    LanguageCode = GetLanguageCode (szLanguageCode);
    if (pszHelp != NULL)
    {
        for (int i = 0; i < TotalHelp; i++)
             free (pszHelp[i]);
        free (pszHelp);
        pszHelp = NULL;
        TotalHelp = 0;
    }     
    /* Help defination */
    p = szLoadSysConfigString (&MyConfig, NULL, "TotalHelp", "0");
    TotalHelp = atoi (p);
    free (p);

    if (TotalHelp != 0)
    {
        pszHelp = (char **) malloc (sizeof (char *) * TotalHelp);
        if (pszHelp == NULL)
            return false;

        for (int i = 0; i < TotalHelp; i++)
        {
            char buf[32];
            sprintf (buf, "Help%d", i + 1);
            pszHelp[i] = szLoadSysConfigString (&MyConfig, NULL, buf, NULL);
        }
    }
    
    p = szLoadSysConfigString (&MyConfig, NULL, "TotalMethod", "0");
    TotalMethod = atoi (p);
    free (p);
    if (TotalMethod != 0)
    {
        if (TotalMethod > NR_INPUT)
            TotalMethod = NR_INPUT;

        for (int i = 0; i < TotalMethod; i++)
        {
            char buf[32];

            sprintf (buf, "MethodModule%d", i + 1);
            aInputMethod[i].szImmModule = szLoadSysConfigString 
                                              (&MyConfig, NULL, buf, NULL);

            sprintf (buf, "MethodTable%d", i + 1);
            aInputMethod[i].szImmTable = szLoadSysConfigString 
                                              (&MyConfig, NULL, buf, NULL);

            // sprintf (buf, "MethodType%d", i + 1);
            // p = szLoadSysConfigString (&MyConfig, NULL, buf, NULL);
            aInputMethod[i].type = LanguageCode; //atoi (p);
            // free (p);

            sprintf (buf, "MethodName%d", i + 1);
            aInputMethod[i].szMethodName = szLoadSysConfigString 
                                      (&MyConfig, NULL, buf, NULL);
        }
    }

    /* SysMenu Support */
    szSysMenu = szLoadSysConfigString (&MyConfig, szSysMenu, "SysMenu", "");
    return true;
}

bool CMyConfig::ChangeLanguageCode (char *szNewLanguageCode)
{
    if (szLanguageCode != NULL)
        free (szLanguageCode);
    szLanguageCode = strdup (szNewLanguageCode);
    LoadLanguageCode ();
    return true;
}

void CMyConfig::PrintAll ()
{
    printf ("\nszLanguageCode = %s\n", szLanguageCode);
    printf ("TotalMethod = %d\n", TotalMethod);
    printf ("szFullChar = %s\n", szFullChar);
    printf ("szSingleChar = %s\n", szSingleChar);
    printf ("szSingleSymbol = %s\n", szSingleSymbol);
    printf ("szFullSymbol = %s\n", szFullSymbol);
    printf ("szVersionInfo = %s\n", szVersionInfo);
    printf ("szLanguageCode = %s\n", szLanguageCode);
    printf ("szFirstMessage = %s\n", szFirstMessage);
    printf ("szFullSymbolLeftMark = %s\n", szFullSymbolLeftMark);
    printf ("szFullSymbolRightMark = %s\n", szFullSymbolRightMark);
    printf ("szSingleSymbolLeftMark = %s\n", szSingleSymbolLeftMark);
    printf ("szSingleSymbolRightMark = %s\n", szSingleSymbolRightMark);
}

#ifdef __MYCONFIG_DEBUG__
int main ()
{
    CMyConfig *pMyConfig = new CMyConfig (UNICON_LIB"/unicon.ini", "GB");
    delete pMyConfig;
}
#endif

