/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */


/* System Config Management */

/* Similar to Microsoft Windows 
   1). Syntex definations
       ^[xxx]           -- Group
       =                -- equal
       empty line       -- group defination end
   2). Comments

 */

struct SysConfigCap
{
    char *szName;
    char *szValue;
};

class CSysConfigGroup
{
public:
    char *szGroupName;
    SysConfigCap *pOurCap;
    int nTotalCap; 
public:
    CSysConfigGroup (const char *szGroupName = NULL);
    ~CSysConfigGroup ();

    /* Group name operation */
    char *szGetGroupName ();
    bool SetGroupName (const char *szGroupName);

    SysConfigCap *pFindItem (const char *szItemName);
    bool bChangeItem (const char *szItemName, const char *szNewValue);
    bool bAddItem (const char *szItemName, const char *szValue);
    char *szGetItemValue (const char *szItemName);

    friend ifstream & operator >> (ifstream &a, CSysConfigGroup &b);
    friend ofstream & operator << (ofstream &a, CSysConfigGroup &b);
};

class CSysConfig
{
private:
    char *szFileName;
    CSysConfigGroup **ppOurGroup;
    int nTotalGroup;
    int modified;
public:
    CSysConfig (const char *szFileName = NULL);
    ~CSysConfig ();
    bool bSave (const char *szFileName);

    /* Group Operation */
    CSysConfigGroup *AddGroup (CSysConfigGroup *p);
    CSysConfigGroup *pFindGroup (const char *szGroup);
    CSysConfigGroup *pCreateNewGroup (const char *szGroup);

    /* Item Operation */
    bool bWriteCapItem (const char *szGroup, const char *capName, const char *value);
    char *szReadCapItem (const char *szGroup, const char *szCapName);
};

