/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */
/* MyInputArea.cpp: */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <MyInputArea.hpp>

CMyInputArea::CMyInputArea (int nTty0, CMyConfig *pMyConfig, 
                            CUniKey *pCMyVideo0)
{
    pTheConfig = pMyConfig;
    pCMyVideo = pCMyVideo0;
    pCMyVideo->GetVideoInfo (&MyVideoInfo);
    szMethod = NULL;
    SelMaxLen = MyVideoInfo.pixel_width/MyVideoInfo.font_width - 2;
    x1 = 0; 
    x2 = SelMaxLen;
    y1 = y2 = MyVideoInfo.pixel_height/MyVideoInfo.font_height - 1;
    fgcolor = INPUT_FGCOLOR;
    bgcolor = INPUT_BGCOLOR;
    markcolor = MARK_FGCOLOR;
    which = 0;
    nTty = nTty0;
//    ChineseInput_X = 8;
    ShowFirstStartMsg ();
}

/*
CMyInputArea::CMyCandArea (int nTty0, CMyConfig *pMyConfig, 
                            CUniKey *pCMyVideo0)
{
    pTheConfig = pMyConfig;
    pCMyVideo = pCMyVideo0;
    pCMyVideo->GetVideoInfo (&MyVideoInfo);
    szMethod = NULL;
    SelMaxLen = MyVideoInfo.pixel_width/MyVideoInfo.font_width - 2;
    x1 = 0; 
    x2 = SelMaxLen;
    y1 = y2 = MyVideoInfo.pixel_height/MyVideoInfo.font_height - 2;
    fgcolor = INPUT_FGCOLOR;
    bgcolor = INPUT_BGCOLOR;
    markcolor = MARK_FGCOLOR;
    which = 0;
    nTty = nTty0;
//    ChineseInput_X = 8;
    ShowFirstStartMsg ();
}
*/

CMyInputArea::~CMyInputArea ()
{
    if (szMethod != NULL)
        free (szMethod);
}

void CMyInputArea::ResetMyConfig (CMyConfig *pMyConfig)
{
    pTheConfig = pMyConfig;
//    printf ("nTty = %d \n", nTty);
//    pTheConfig->PrintAll ();
//    RefreshInputArea (0, 0);
}

/* Display System Menu */
void CMyInputArea::ShowSysMenu ()
{
    char *p = pTheConfig->szSysMenu;
    ClearInputArea ();
    WriteColorTextToInputArea (p);
}

/* Display Help Item */
void CMyInputArea::ShowHelpItem (int n)
{
    n %= pTheConfig->TotalHelp;
    if(n<0)
	n=pTheConfig->TotalHelp+n;
    char *p = pTheConfig->pszHelp[n];
    ClearInputArea ();
    WriteColorTextToInputArea (p);
}

/* Different Language Code support */
void CMyInputArea::SetNewLanguageCode (char *szNewCode)
{
    pTheConfig->ChangeLanguageCode (szNewCode);
}

void CMyInputArea::DisplaySelection (char *szStr)
{
    strcpy (SelectionBuffer, szStr);
}

void CMyInputArea::ClearSelection ()
{
    strcpy (SelectionBuffer, "");
}

//void CMyInputArea::DisplayCand (char *szStr)
void CMyInputArea::DisplayInput (char *szStr)
{
    strcpy (InputBuffer, szStr);
}

//void CMyInputArea::ClearCand ()
void CMyInputArea::ClearInput ()
{
    strcpy (InputBuffer, "");
}

//void CMyInputArea::ClearCandArea ()
void CMyInputArea::ClearInputArea ()
{
    pCMyVideo->InputAreaClear (0, y1, SelMaxLen + 2, y2, fgcolor);
}

//void CMyInputArea::WriteToCandArea (char *szStr)
void CMyInputArea::WriteToInputArea (char *szStr)
{
    ClearInputArea ();
    pCMyVideo->InputAreaOutput (x1, szStr, INPUT_FGCOLOR,INPUT_BGCOLOR);
}

//void CMyInputArea::WriteColorTextToCandArea (char *szStr)
void CMyInputArea::WriteColorTextToInputArea (char *szStr)
{
    ClearInputArea ();
    pCMyVideo->InputAreaOutput (x1, szStr, INPUT_FGCOLOR,INPUT_BGCOLOR);
/*
    char *buf, *p;
    int j, nBegin = 0;
 
    buf = (char *) malloc (strlen (szStr) + 1);  
    if (buf == NULL)
    {
        printf ("no enough memory to run ....\n");
        exit (-1);
    }

    p = szStr;
    do
    {
        for (j = 0; *p; p++, j++)
        {
            if ((isalnum (*p) || *p == ' ' || *p == '.' || *p == '\t'))
                break;
            buf[j] = *p;
        }
        buf[j] = '\0';

        pCMyVideo->InputAreaOutput(nBegin, buf, INPUT_FGCOLOR,INPUT_BGCOLOR);

        nBegin += strlen (buf) + 1;
        if (*p == '\t')
        {
             p ++;
             nBegin += 4;
        }

        for (j = 0; *p; p++, j++)
        {
            if (!(isalnum (*p) || *p == ' ' || *p == '.' ) || *p == '\t')
                buf[j] = *p;
        }
        buf[j] = '\0';

        pCMyVideo->InputAreaOutput(nBegin, buf, MARK_FGCOLOR,INPUT_BGCOLOR);

        nBegin += strlen (buf) + 1;
        if (*p == '\t')
        {
             p ++;
             nBegin += 4;
        }
    }
    while (*p);
    free (buf);
*/
}

//void CMyInputArea::RefreshCandArea (int which, int IsFullSymbol)
void CMyInputArea::RefreshInputArea (int which, int IsFullSymbol)
{
   char buf[256];
   char *p;
   ClearInputArea ();
   if (which == 3)
   {
       if (IsFullSymbol == 1)
           sprintf (buf, "%s%s%s", 
               pTheConfig->szFullSymbolLeftMark,
               pTheConfig->szFullChar,
               pTheConfig->szFullSymbolRightMark);
       else
           sprintf (buf, "%s%s%s", 
               pTheConfig->szSingleSymbolLeftMark,
               pTheConfig->szFullChar,
               pTheConfig->szSingleSymbolRightMark);
       pCMyVideo->InputAreaOutput (x1, buf, INPUT_FGCOLOR,INPUT_BGCOLOR);
   }
   else if (which == 0)
   {
       sprintf(buf, " [%s] ", pTheConfig->szSingleChar);
       pCMyVideo->InputAreaOutput (x1, buf, INPUT_FGCOLOR,INPUT_BGCOLOR);

       if (IsFullSymbol == 1)
           p = pTheConfig->szFullSymbol;
       else
           p = pTheConfig->szSingleSymbol;
       sprintf(buf, "[%s]", p);
       pCMyVideo->InputAreaOutput (x1 + 8, buf, INPUT_FGCOLOR,INPUT_BGCOLOR);

       sprintf(buf, "%s", pTheConfig->szVersionInfo);
       pCMyVideo->InputAreaOutput (
                   (x2 - x1 - strlen (buf))/2, buf,
                   TITLE_FGCOLOR, INPUT_BGCOLOR);
       sprintf(buf, "[tty%d] %s", nTty, pTheConfig->szLanguageCode);
       pCMyVideo->InputAreaOutput (
                    x2 - x1 - strlen (buf) - 1,
                    buf, INPUT_FGCOLOR, INPUT_BGCOLOR);
   }
   else if (which == 1)
   {
       if (IsFullSymbol == 1)
           sprintf (OutputBuffer, "%s%s%s %s %s",
                pTheConfig->szFullSymbolLeftMark,
                szMethod,
                pTheConfig->szFullSymbolRightMark,
                InputBuffer, SelectionBuffer);
       else
           sprintf (OutputBuffer, "%s%s%s %s %s", 
               pTheConfig->szSingleSymbolLeftMark,
               szMethod,
               pTheConfig->szSingleSymbolRightMark,
               InputBuffer, SelectionBuffer);
       /* 显示输入键和选择的字词 */
       pCMyVideo->InputAreaOutput (x1, OutputBuffer, fgcolor, bgcolor);
    }
//    printf ("nTty = %d,  pTheConfig = 0x%x \n", nTty, (long) pTheConfig);
}

void CMyInputArea::SetNewMethod (char *szNewMethod)
{
    szMethod = strdup (szNewMethod);
}

char * CMyInputArea::GetNewMethod ()
{
    return szMethod;
}

void CMyInputArea::ShowFirstStartMsg ()
{
    ClearInputArea ();
    pCMyVideo->InputAreaOutput(1, pTheConfig->szFirstMessage,
                               INPUT_FGCOLOR,INPUT_BGCOLOR);
}

void CMyInputArea::SetSelectDisplay (char *szStr)
{
    strcpy (SelectionBuffer, szStr);
}

void CMyInputArea::SetInputDisplay (char *szStr)
{
    strcpy (InputBuffer, szStr);
}
 
/* MyInputArea.cpp: */
/*========>8 END 8<========*/
