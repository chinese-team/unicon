/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#ifndef __UNIKEY_HPP__
#define __UNIKEY_HPP__

#include <MyTypes.h>
#include <unikey.h>

class CUniKey
{
private:
    int fd;
    MyVideoInfo_T MyVideoInfo;
    int ChineseInput_X;
    int ChineseInput_Y;
    int SelectionXMax;
private:
//    int GetCurrentTTY ();
    int TestFBExist ();
    /* Key Hooker */
    int WriteOsKeys (TTY_KEY_T *ui);
    int ReadOsKeys (TTY_KEY_T *ui);
public:
    CUniKey (char *szDevice, int resize);
    ~CUniKey ();

    /* Key Hooker */
    int WriteKeys (int nTty, char *szKey, int total);
    int ReadKeys (TTY_KEY_T *ui);

    /* System Dependent Input Display */
    void InputAreaSPut (int x, int y, u_char ch, 
                        VColor fg, VColor bg);
    void InputAreaWPut (int x, int y, 
                u_char ch1, u_char ch2, VColor fg, VColor bg);
    void InputAreaClear (int x1, int y1, int x2, int y2, VColor color);
    void InputAreaOutput (int x, char *Str, VColor fg, VColor bg);

    /* Video Info Operations */
    void GetVideoInfo (MyVideoInfo_T *p);
    void SetVtChangeSize ();
    void SetVtRestoreSize ();
    int  GetVideoPixelWidth ();
    int  GetVideoPixelHeight ();
    int  GetFontWidth ();
    int  GetFontHeight ();
    void ResizeTerm (int nRow);
    void SetCurrentFont (int tty, int font_type);

    /* Public function */
    int GetCurrentTTY ();
};

#endif

