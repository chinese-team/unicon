/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

int WriteFullSymbol (u_char c, char *buf)
{
    switch (c)
    {
        case '"':
        {
            static int firstchar = 0;
            if (firstchar == 0)
                strcpy (buf, "��");
            else
                strcpy (buf, "��");
            firstchar = firstchar == 0 ? 1 : 0;
            break;
        }
        case ':':
            strcpy (buf, "��");
            break;
        case ';':
            strcpy (buf, "��");
            break;
        case '<':
            strcpy (buf, "��");
            break;
        case '>':
            strcpy (buf, "��");
            break;
        case ',':
            strcpy (buf, "��");
            break;
        case '.':
            strcpy (buf, "��");
            break;
        case '?':
            strcpy (buf, "��");
            break;
        case '/':
            strcpy (buf, "��");
            break;
        case '|':
            strcpy (buf, "��");
            break;
        case '\\':
            strcpy (buf, "��");
            break;
        case '~':
            strcpy (buf, "��");
            break;
        case '`':
            strcpy (buf, "��");
            break;
        case '!':
            strcpy (buf, "��");
            break;
        case '@':
            strcpy (buf, "��");
            break;
        case '#':
            strcpy (buf, "��");
            break;
        case '$':
            strcpy (buf, "��");
            break;
        case '%':
            strcpy (buf, "��");
            break;
        case '^':
            strcpy (buf, "����");
            break;
        case '&':
            strcpy (buf, "��");
            break;
        case '*':
            strcpy (buf, "��");
            break;
        case '(':
            strcpy (buf, "��");
            break;
        case ')':
            strcpy (buf, "��");
            break;
        case '-':
            strcpy (buf, "��");
            break;
        case '_':
            strcpy (buf, "����");
            break;
        case '{':
            strcpy (buf, "��");
            break;
        case '}':
            strcpy (buf, "��");
            break;
        case '[':
            strcpy (buf, "��");
            break;
        case ']':
            strcpy (buf, "��");
            break;
        default:
            return 0;
    }
    return 1;
}

int WriteFullChar (u_char c, char *buf)
{
    static char fullchar[] =
        "���������磥����������������������������������������������������"
        "�����£ãģţƣǣȣɣʣˣ̣ͣΣϣУѣңӣԣգ֣ףأ٣ڡ��ܡ��ޡ�"
        "��������������������������������������������  ";

    if (isalnum (c) != 0 || c == ' ')
    {
        u_char key = (c - ' ') << 1;
        memcpy (buf, fullchar + key, 2);
        buf[2] = '\0';
        return 1;
    }
    return 0;
}
