/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

/*****************************************************************************
 *
 *       ImmClient.c  ====    Input Method Modules Interface 
 *
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <TLS_LibMain.hpp>
#include <ImmClient.h>
#include <TLC_Utils.h>

ImmServer_T IMM_OpenClient (char *szIpAddr, u_short port)
{
    return (ImmServer_T) TCP_Connect (szIpAddr, port);
}

int IMM_CloseClient (ImmServer_T ImmServer)
{
    TCP_Disconnect ((long) ImmServer);
    return 1;
}

/* Phrase Table File Operation */
IMM *IMM_OpenInput (ImmServer_T ImmServer, char *szImmModule,
                    char *szImmTable, u_long type)
{
   IMM *p = (IMM *) malloc (sizeof (IMM));
   if (p == NULL)
      return NULL;
   // printf (" %ld, %s, %s, %ld\n", 
   //          ImmServer, szImmModule, szImmTable, type);
   TLS_CServerMain  *pImmServer = (TLS_CServerMain *) ImmServer;
   p->handle = pImmServer->OpenServer (szImmModule, szImmTable, type);
   p->pCImmServer = ImmServer;
   if (p->handle == 0)
   {
       free (p);
       return NULL;
   }
   return p;
}

int IMM_CloseInput (IMM *p)
{
    TLS_CServerMain  *pImmServer = (TLS_CServerMain *) p->pCImmServer;
    int b = pImmServer->CloseServer (p->handle);
    free (p);
    return b;
}

/* Input Method Operations */
int IMM_ResetInput (IMM *p)
{
    TLS_CServerMain  *pImmServer = (TLS_CServerMain *) p->pCImmServer;
    return pImmServer->ResetInput (p->handle);
}

int  IMM_SetInputMode  (IMM *pImm, long mode)
{
    TLS_CServerMain *pImmServer = (TLS_CServerMain *) pImm->pCImmServer;
    return pImmServer->SetInputMode (pImm->handle, mode);
}

int IMM_KeyFilter (IMM *pImm, u_char key, char *buf, int *len)
{
    TLS_CServerMain *pImmServer = (TLS_CServerMain *) pImm->pCImmServer;
    return pImmServer->KeyFilter (pImm->handle, key, buf, len);
}

/* Input Area Configuration & Operation */
int IMM_ConfigInputArea (IMM *pImm, int SelectionLen)
{
    TLS_CServerMain *pImmServer = (TLS_CServerMain *) pImm->pCImmServer;
    return pImmServer->ConfigInputArea (pImm->handle, SelectionLen);
}

int IMM_GetInputDisplay (IMM *pImm, char *buf, long buflen)
{
    TLS_CServerMain *pImmServer = (TLS_CServerMain *) pImm->pCImmServer;
    return pImmServer->GetInputDisplay (pImm->handle, buf, buflen);
}

int IMM_GetSelectDisplay (IMM *pImm, char *buf, long buflen)
{
    TLS_CServerMain *pImmServer = (TLS_CServerMain *) pImm->pCImmServer;
    return pImmServer->GetSelectDisplay (pImm->handle, buf, buflen);
}

int IMM_ChangePhraseItem (IMM *pImm, u_long n, 
               char *szCode, char *szPhrase, u_long freq)
{
    TLS_CServerMain  *pImmServer = (TLS_CServerMain *) pImm->pCImmServer;
    return pImmServer->SetPhraseItem (pImm->handle, n, szCode, szPhrase, freq); 
}

/* User Phrase Interface */
int IMM_AddUserPhrase (IMM *pImm, 
                char *szCode, char *szPhrase, u_long freq)
{
    TLS_CServerMain  *pImmServer = (TLS_CServerMain *) pImm->pCImmServer;
    return pImmServer->AddUserPhrase (pImm->handle, szCode, szPhrase, freq); 
}

int IMM_FlushUserPhrase (IMM *pImm)
{
    TLS_CServerMain  *pImmServer = (TLS_CServerMain *) pImm->pCImmServer;
    return pImmServer->FlushUserPhrase (pImm->handle);
}

