
/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#include <iostream.h>
#include <stdio.h>
#include <stdlib.h>
#include <Phrase.h>
#include <TLC_ImmClient.h>
#include <TLC_ImmModule.h>

#ifdef __DLL_SUPPORT__
#ifdef  __cplusplus
extern "C" {
#endif
    extern long TCP_Connect (char *szIpAddr, short Port);
    extern int TCP_Disconnect (long handle);
    extern int LibOpen ();
    extern int LibRelease ();
#ifdef  __cplusplus
}
#endif
#endif

int main (int argc, char **argv)
{
    char mbuf[256];
    if (argc < 3)
    {
        cout << argv[0] << " " << "<ip> " << "<port>" << "\n";
        exit (-1);
    }        
#ifdef __DLL_SUPPORT__
    LibOpen ();
#endif
    ImmServer_T t = IMM_OpenClient (argv[1], atoi (argv[2]));
    cout << "OpenClient ()\n";

    IMM *MyImm = IMM_OpenInput (t, "../ImmModules/CCE/cce_pinyin.so",
                                "../datas", IMM_CCE);
    cout << "immOpen (....)\n";

    do
    {
        char ch;
        printf ("please input a key:(q == exit):");
        scanf ("%c", &ch);
        if (ch == 'q')
            break;
        if (ch == '\r' || ch == '\n')
            continue;
        long n = IMM_KeyPressed (MyImm, ch);
        cout << "in test.cpp:total:" << n << "\n";
        IMM_GetInputDisplay (MyImm, mbuf, sizeof (mbuf));
        cout << "Input::" << mbuf << "\n";
        IMM_GetSelectDisplay (MyImm, mbuf, sizeof (mbuf));
        cout << "Selection::" << mbuf << "\n";
/*
        for (long i = 0; i < n; )
        { 
            char temp[8192];
            PhraseItem MyPhrase[20]; 
            long tt = IMM_GetPhraseItems (MyImm, i, 20, 
                                     MyPhrase, temp, sizeof (temp));
            for (int j = 0; j < tt; j++)
            {
                fprintf (stderr, "%s,", MyPhrase[j].szPhrase);
                fflush (stderr);
            }
            i += tt;
            fprintf (stderr, "\n");
        }
*/
    }
    while (1);
    IMM_CloseInput (MyImm);
    cout << "immClose ()\n";

    IMM_CloseClient (t);
    cout << "bCloseImmClient ()\n";
#ifdef __DLL_SUPPORT__
    LibRelease ();
#endif
    return 1;
}

