/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#ifndef __MEMFILE_HPP__
#define __MEMFILE_HPP__

#include <stdio.h>
#include <stdlib.h>

typedef char * PSTR;
class TLS_CMemFile
{
private:
   char *buf;
   long max;
   long pos;
   long len;
   bool bAllocate;
public:    
    TLS_CMemFile (char *buf, u_long len, u_long max);
    TLS_CMemFile (u_long max);
    ~TLS_CMemFile ();
    int fseek (long offset, int whence);
    long ftell ();
    size_t fread (void *ptr, size_t size, size_t nmemb);
    size_t  fwrite (void  *ptr,  size_t  size, size_t nmemb);
    void rewind ();
    /* buf operation */
    char *pGetBuf ();
    char *pGetCurrentPos ();
    u_long GetBufLen ();
    u_long GetMax ();
    void SetBufLen (int n);    

    /* operator overload */
    friend TLS_CMemFile & operator >> (TLS_CMemFile &in, long &b);
    friend TLS_CMemFile & operator >> (TLS_CMemFile &in, short &b);
    friend TLS_CMemFile & operator >> (TLS_CMemFile &in, char &b);
    friend TLS_CMemFile & operator >> (TLS_CMemFile &in, PSTR &p);
    friend TLS_CMemFile & operator << (TLS_CMemFile &in, long b);
    friend TLS_CMemFile & operator << (TLS_CMemFile &in, short b);
    friend TLS_CMemFile & operator << (TLS_CMemFile &in, char b);
    friend TLS_CMemFile & operator << (TLS_CMemFile &in, PSTR p);
};

#endif

