/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#include <iostream.h>
#include <stdio.h>
#include <string.h>
#include <dlfcn.h>
#include <TLS_ImmOp.hpp>

TLS_CImmOp::TLS_CImmOp ()
{
}

TLS_CImmOp::~TLS_CImmOp ()
{
}

bool TLS_CImmOp::LoadImm (char *szImmModule, long type, ImmOp_T *p)
{
    void *ldso = dlopen(szImmModule, RTLD_LAZY);
    if (ldso == NULL)
    {
        printf ("dlopen (%s) failed\n", szImmModule);
        printf ("error::%s\n", dlerror ());
        return false;
    }
    ImmOperation *q = (ImmOperation *) dlsym(ldso, "ImmOp_Ptr");
    if (q == NULL)
    {
        printf ("failed to get the ImmOp_ptr\n");
        return false;
    }
    p->handle = ldso;
    p->pImmOp = q;
    return true;
}

void TLS_CImmOp::UnloadImm (ImmOp_T *p)
{
     p->pImmOp = NULL;
     dlclose (p->handle);
     p->handle = NULL;
     free (p);
}

/* Support Dynamic Load and Free */
ImmOp_T *TLS_CImmOp::OpenImm (char *szImmModule, long type)
{
    ImmOp_T *p;
    p = (ImmOp_T *) malloc (sizeof (ImmOp_T));
    if (p == NULL)
        return NULL;
    if (LoadImm (szImmModule, type, p) == false)
        return NULL;
    return p;
}

void TLS_CImmOp::CloseImm (ImmOp_T *p)
{
    UnloadImm (p);
}

