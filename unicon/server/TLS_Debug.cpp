/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#include <stdio.h>
#include <stdlib.h>
#include <iostream.h>
#include <fstream.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <TLS_Debug.hpp>
TLS_CDebug::TLS_CDebug (char *szFileName, int mode = 0)
{
    if (szFileName == NULL)
    {
        bDebugToFile = 0;
        fp = stderr;
    }
    else
    {
        bDebugToFile = 1;
        if (mode == 0)
            fp = fopen (szFileName, "wt");
        else 
            fp = fopen (szFileName, "at");
        if (fp == NULL)
        {
            printf ("Can't open %s\n", szFileName);
            exit (-1);
        }
    }
}

TLS_CDebug::~TLS_CDebug ()
{
    if (bDebugToFile == true)
        fclose (fp);
}
    /* printf support */
int TLS_CDebug::printf (const char *format, ...)
{
    va_list args;
    char buf[512];

    va_start(args, format);
    int i = vsprintf (buf, format, args); 
    va_end(args);

    fprintf (fp, "(printf) %s\n", buf);
    return i;
}
    /* operator overload */
TLS_CDebug & operator << (TLS_CDebug &in, long b)
{
     fprintf (in.fp, "(long) %ld\n", b);
     return in;
}

TLS_CDebug & operator << (TLS_CDebug &in, u_long b)
{
     fprintf (in.fp, "(u_long) %ld\n", b);
     return in;
}

TLS_CDebug & operator << (TLS_CDebug &in, short b)
{
     fprintf (in.fp, "(short) %d\n", b);
     return in;
}

TLS_CDebug & operator << (TLS_CDebug &in, u_short b)
{
     fprintf (in.fp, "(u_short) %d\n", b);
     return in;
}

TLS_CDebug & operator << (TLS_CDebug &in, char b)
{
     fprintf (in.fp, "(char) %d\n", b);
     return in;
}

TLS_CDebug & operator << (TLS_CDebug &in, u_char b)
{
     fprintf (in.fp, "(u_char) %d\n", b);
     return in;
}

TLS_CDebug & operator << (TLS_CDebug &in, PSTR p)
{
     fprintf (in.fp, "(u_char) %s\n", p);
     return in;
}

#ifdef __CDEBUG_DEBUG__
int main ()
{
    TLS_CDebug me;
    me << "this is a test";
    TLS_CDebug my ("test.dat");
    my << "this is a test";
}
#endif

