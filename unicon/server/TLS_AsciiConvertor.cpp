/*
    Copyright (C) 1999 by  XCIN TEAM

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    For any question or suggestion, please mail to xcin mailing-list:
    xcin@linux.org.tw, or the maintainer Tung-Han Hsieh: thhsieh@linux.org.tw
*/      

#include <stdio.h>
#include <string.h>
#include "TLS_AsciiConvertor.hpp"

static xkeymap_t fullchar_template[] = {	/* sorted by ascii code */
	{' ',		{{'\0'}}},	/*     */
	{'!',		{{'\0'}}},	/*  !  */
	{'"',		{{'\0'}}},	/*  "  */
	{'#',		{{'\0'}}},	/*  #  */
	{'$',		{{'\0'}}},	/*  $  */
	{'%',		{{'\0'}}},	/*  %  */
	{'&',		{{'\0'}}},	/*  &  */
	{'\'',		{{'\0'}}},	/*  '  */
	{'(',		{{'\0'}}},	/*  (  */
	{')',		{{'\0'}}},	/*  )  */
	{'*',		{{'\0'}}},	/*  *  */
	{'+',		{{'\0'}}},	/*  +  */
	{',',		{{'\0'}}},	/*  ,  */
	{'-',		{{'\0'}}},	/*  -  */
	{'.',		{{'\0'}}},	/*  .  */
	{'/',		{{'\0'}}},	/*  /  (undefined) */
	{'0',		{{'\0'}}},	/*  0  */
	{'1',		{{'\0'}}},	/*  1  */
	{'2',		{{'\0'}}},	/*  2  */
	{'3',		{{'\0'}}},	/*  3  */
	{'4',		{{'\0'}}},	/*  4  */
	{'5',		{{'\0'}}},	/*  5  */
	{'6',		{{'\0'}}},	/*  6  */
	{'7',		{{'\0'}}},	/*  7  */
	{'8',		{{'\0'}}},	/*  8  */
	{'9',		{{'\0'}}},	/*  9  */
	{':',		{{'\0'}}},	/*  :  */
	{';',		{{'\0'}}},	/*  ;  */
	{'<',		{{'\0'}}},	/*  <  */
	{'=',		{{'\0'}}},	/*  =  */
	{'>',		{{'\0'}}},	/*  >  */
	{'?',		{{'\0'}}},	/*  ?  */
	{'@',		{{'\0'}}},	/*  @  */
	{'A',		{{'\0'}}},	/*  A  */
	{'B',		{{'\0'}}},	/*  B  */
	{'C',		{{'\0'}}},	/*  C  */
	{'D',		{{'\0'}}},	/*  D  */
	{'E',		{{'\0'}}},	/*  E  */
	{'F',		{{'\0'}}},	/*  F  */
	{'G',		{{'\0'}}},	/*  G  */
	{'H',		{{'\0'}}},	/*  H  */
	{'I',		{{'\0'}}},	/*  I  */
	{'J',		{{'\0'}}},	/*  J  */
	{'K',		{{'\0'}}},	/*  K  */
	{'L',		{{'\0'}}},	/*  L  */
	{'M',		{{'\0'}}},	/*  M  */
	{'N',		{{'\0'}}},	/*  N  */
	{'O',		{{'\0'}}},	/*  O  */
	{'P',		{{'\0'}}},	/*  P  */
	{'Q',		{{'\0'}}},	/*  Q  */
	{'R',		{{'\0'}}},	/*  R  */
	{'S',		{{'\0'}}},	/*  S  */
	{'T',		{{'\0'}}},	/*  T  */
	{'U',		{{'\0'}}},	/*  U  */
	{'V',		{{'\0'}}},	/*  V  */
	{'W',		{{'\0'}}},	/*  W  */
	{'X',		{{'\0'}}},	/*  X  */
	{'Y',		{{'\0'}}},	/*  Y  */
	{'Z',		{{'\0'}}},	/*  Z  */
	{'[',		{{'\0'}}},	/*  [  */
	{'\\',		{{'\0'}}},	/*  \  (undefined) */
	{']',		{{'\0'}}},	/*  ]  */
	{'^',		{{'\0'}}},	/*  ^  */
	{'_',		{{'\0'}}},	/*  _  (undefined) */
	{'`',		{{'\0'}}},	/*  `  */
	{'a',		{{'\0'}}},	/*  a  */
	{'b',		{{'\0'}}},	/*  b  */
	{'c',		{{'\0'}}},	/*  c  */
	{'d',		{{'\0'}}},	/*  d  */
	{'e',		{{'\0'}}},	/*  e  */
	{'f',		{{'\0'}}},	/*  f  */
	{'g',		{{'\0'}}},	/*  g  */
	{'h',		{{'\0'}}},	/*  h  */
	{'i',		{{'\0'}}},	/*  i  */
	{'j', 		{{'\0'}}},	/*  j  */
	{'k',		{{'\0'}}},	/*  k  */
	{'l',		{{'\0'}}},	/*  l  */
	{'m',		{{'\0'}}},	/*  m  */
	{'n',		{{'\0'}}},	/*  n  */
	{'o',		{{'\0'}}},	/*  o  */
	{'p',		{{'\0'}}},	/*  p  */
	{'q',		{{'\0'}}},	/*  q  */
	{'r',		{{'\0'}}},	/*  r  */
	{'s',		{{'\0'}}},	/*  s  */
	{'t',		{{'\0'}}},	/*  t  */
	{'u',		{{'\0'}}},	/*  u  */
	{'v',		{{'\0'}}},	/*  v  */
	{'w',		{{'\0'}}},	/*  w  */
	{'x',		{{'\0'}}},	/*  x  */
	{'y',		{{'\0'}}},	/*  y  */
	{'z',		{{'\0'}}},	/*  z  */
	{'{',		{{'\0'}}},	/*  {  */
	{'|',		{{'\0'}}},	/*  |  */
	{'}',		{{'\0'}}},	/*  }  */
	{'~',		{{'\0'}}},	/*  ~  */
	{0L,		{{'\0'}}}
};

TLS_CAsciiConvertor::TLS_CAsciiConvertor (char *szFileName)
{
    FILE *fp;
    int len;
    char buf[40];

    fullchar = (xkeymap_t *) malloc (sizeof (fullchar_template));
    if (fullchar == NULL)
    {
        printf ("fatal error::no enough memory to run.....\n");
        exit (-1);
    }
    memcpy (fullchar, fullchar_template, sizeof (fullchar_template));

    fp = fopen (szFileName, "rb");
    if (fp == NULL)
    {
        printf ("fatal error!!!\n");
        printf ("Can't open %s\n", szFileName);
        exit (-1);
    }
        
    if (fread(buf, sizeof(char), MODULE_ID_SIZE, fp) != MODULE_ID_SIZE)
    {
        printf ("invalid tab file: \n");
        exit (-1);
    }
    len = sizeof(SYSCIN_VERSION);
    if (fread(buf, len, 1, fp) != 1)
    {
        printf ("invalid sys.tab version\n");
        exit (-1);
    }
    if (fread(inpn_english, sizeof(char), CIN_CNAME_LENGTH, fp)
                != CIN_CNAME_LENGTH ||
        fread(inpn_sbyte, sizeof(char), CIN_CNAME_LENGTH, fp)
                != CIN_CNAME_LENGTH ||
        fread(inpn_2bytes, sizeof(char), CIN_CNAME_LENGTH, fp)
                != CIN_CNAME_LENGTH ||
        fread(ascii, sizeof(wch_t), N_ASCII_KEY, fp) != N_ASCII_KEY ||
        fread(ccp, sizeof(charcode_t), WCH_SIZE, fp) != WCH_SIZE)
    {
        printf ("sys.tab reading error.\n");
        exit (-1);
    }
    fclose(fp);
    fullascii_init(ascii);
}

TLS_CAsciiConvertor::~TLS_CAsciiConvertor ()
{
    free (fullchar);
}

void TLS_CAsciiConvertor::fullascii_init(wch_t *list)
{
    xkeymap_t *fc=fullchar;
    int i=0;

    for (; fc->xkey; fc++)
	fc->wch.wch = list[i++].wch;
}

char *TLS_CAsciiConvertor::szFullAsciiKeyStroke (u_char key)
{
    xkeymap_t *fc=fullchar;

    while (fc->xkey != 0) {
	if (key == fc->xkey) {
	    strncpy(cch, (char *) fc->wch.s, WCH_SIZE);
	    cch[WCH_SIZE] = '\0';
	    return cch;
	}
	fc ++;
    }
    return NULL;
}

int IsSymbol (u_char key)
{
    char *szSymbol = ";:'\"<,>.?/[{]{";
    int i, len = strlen (szSymbol);
    for (i = 0; i < len; i++)
       if (key == szSymbol[i])
           return 1;
    return 0;
}

char *TLS_CAsciiConvertor::szFullCharKeyStroke (u_char key)
{
    if (IsSymbol (key) == 0)
        return szFullAsciiKeyStroke (key);
    return NULL;
}

char *TLS_CAsciiConvertor::szFullSymbolKeyStroke (u_char key)
{
    if (IsSymbol (key) == 1)
        return szFullAsciiKeyStroke (key);
    return NULL;
}

