#!/bin/bash
echo Installing unicon startup script into system directory...
install -m 755 -o root -g root unicon-start /usr/bin/unicon-start
install -m 755 -o root -g root unicon-init /etc/rc.d/init.d/unicon
chkconfig --add unicon
chkconfig unicon on
