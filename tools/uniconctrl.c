/* Mon Oct 16 2000 Go Taniguchi <go@turbolinux.co.jp> */

#define MAIN 1

#include <stdio.h>
#include <sys/ioctl.h>
#include <linux/ioctl.h>
#include <unistd.h>

typedef struct __VtFont_T__
{
	int tty;
	int font_type;
	int input_method_notify;
} VtFont_T;

#define MAJOR_NUM 100
#define UNI_SET_CURRENT_FONT  _IOR(MAJOR_NUM, 11, VtFont_T *)
#define XL_DB_GB       0
#define XL_DB_BIG5     1
#define XL_DB_JIS      2
#define XL_DB_KSCM     3
#define XL_DB_GBK      4
#define UNIKEY_DEV	"/dev/unikey"

int SetUniconFont(int ttyNo, int FontType)
{
	VtFont_T VtFont;
	int fd, ret;

	fd = open (UNIKEY_DEV, 0);
	if (fd < 0) return -2;
	VtFont.tty = ttyNo;
	VtFont.font_type = FontType;
	VtFont.input_method_notify = 0;
	ret = ioctl(fd, UNI_SET_CURRENT_FONT, &VtFont);
	close(fd);
	return(ret);
}

#if MAIN
void PrintUseage(char *arg)
{
	fprintf(stderr, "Useage: %s <--gb or --gbk --big5 --jis --kscm> [ttyX]\n",
		arg);
}

int main(int argc, char **argv)
{
	char *tty;
	int font_type = 0, ttyNo, ret;

	if(argc != 2 && argc != 3){
		PrintUseage(argv[0]);
		exit(-2);
	}
	if (strcmp (argv[1], "--gb") == 0)
		font_type = XL_DB_GB;
	else if (strcmp (argv[1], "--gbk") == 0)
		font_type = XL_DB_GBK;
	else if (strcmp (argv[1], "--big5") == 0)
		font_type = XL_DB_BIG5;
	else if (strcmp (argv[1], "--jis") == 0)
		font_type = XL_DB_JIS;
	else if (strcmp (argv[1], "--kscm") == 0)
		font_type = XL_DB_KSCM;
	else {
		PrintUseage(argv[0]);
		exit(-2);
	}

	tty = ttyname(0);
	ttyNo = atoi(&tty[strlen(tty)-1]) - 1;
	
	if (argc == 3 && strlen(argv[2]) == 4 && strncmp("tty", argv[2], 3) == 0){
		if(argv[2][3] >= '1' && argv[2][3] <= '6')
			ttyNo = atoi(&(argv[2][3]));
			ttyNo--;
	}

	ret = SetUniconFont(ttyNo, font_type);

	if(ret == -2)
		fprintf(stderr, "Can't open %s\n", UNIKEY_DEV);
	if(ret == -1)
		fprintf(stderr, "Set Current Font failed.\n");
	
	return(ret);
}
#endif
