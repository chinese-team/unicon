/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#ifndef __SFONT_HPP__
#define __SFONT_HPP__

#include <mytype.h>
typedef unsigned char u_char;

#define FONT_SIZE         (16/8 * 16)

struct CharFontInfo
{
    u_char c1, c2;
    char buf[FONT_SIZE];
};

class CSFontManager
{
private:
    CharFontInfo *aCharFontInfo;
    int total;
private:
    bool IsCharExist (u_char c1, u_char c2);
    bool DoAddCharFont (u_char c1, u_char c2, u_char *buf);
    bool bWriteOneFontInfo (FILE *fp, u_char c1, u_char c2, 
                            unsigned char buf[FONT_SIZE]);
    bool bWriteIndexFile (FILE *fp);
    bool bWriteDataFile (FILE *fp);
public:
    CSFontManager ();
    ~CSFontManager ();
    bool AddCharFont (u_char c1, u_char c2, u_char *buf);
    bool SortCharFonts ();
    bool bSaveToFile (char *szFilename, int coding_int);
};

#endif
