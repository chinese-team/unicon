/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#ifdef MODULE
#include <linux/module.h>
#include <linux/fb_doublebyte.h>
#else
#include <mytype.h>
#endif

#include "sfont_big5_16.h"
#include "search.c"

static int index_big5(int left, int right)
{
    return SearchInSmallFont (left, right) << 5;
}

static int is_hz_left(int c)
{
	return (c >= 0xa1 && c <= 0xfa);
}

static int is_hz_right(int c)
{
	return (c >= 40 && c <= 0xff);
}

#ifdef MODULE
static struct double_byte db_big5 =
#else
struct double_byte db_big5 =
#endif
{
	0,
	"BIG5",
	is_hz_left,
	is_hz_right,
	index_big5,
	16,16,
        MAX_SFONT_DATA,
        FontData
};

#ifdef MODULE
int init_module(void)
{
       if (UniconFontManager == 0)
            return 1;
       if (UniconFontManager->registerfont (XL_DB_BIG5, &db_big5) == 0)
            return 1;
        return 0;
}
	
void cleanup_module(void)
{
       if (UniconFontManager == 0)
            return;
       UniconFontManager->unregisterfont (XL_DB_BIG5);
}
#endif

